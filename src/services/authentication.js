import { navigate } from '@reach/router';
import { loginEndpoint, logoutEndpoint, serverEndpoint, } from './endpoints';


export const isBrowser = () => typeof window !== 'undefined';
export const isLoggedIn = () => isBrowser() && window.sessionStorage.getItem('authenticated') === 'true';
const setAuthenticated = () => window.sessionStorage.setItem('authenticated', 'true');
export const setNotAuthenticated = () => window.sessionStorage.setItem('authenticated', 'false');

export const initiateLogin = (event) => {
  if (event) {
    event.preventDefault();
  }
  if (isLoggedIn() || !isBrowser()) {
    return;
  }

  window.addEventListener('message', (message) => {
    if (message.origin === serverEndpoint) {
      setAuthenticated();
      navigate('/edit/profile');
    }
  });
  window.open(loginEndpoint);
};
export const notAuthenticated = () => {
  setNotAuthenticated();
  navigate('/unauthorized');
};
export const logout = () => {
  if (!isLoggedIn() || !isBrowser()) {
    return;
  }

  fetch(logoutEndpoint, { credentials: 'include' })
    .then((resp) => {
      if (resp.status >= 200 && resp.status < 300) {
        setNotAuthenticated();
        navigate('/');
      }
    });
};
