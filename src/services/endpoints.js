export const serverEndpoint = process.env.GATSBY_API_SERVER_ENDPOINT;
export const authorizedEndpoint = `${serverEndpoint}/authenticated-user`;
export const loginEndpoint = `${serverEndpoint}/oauth/login/auth-zero`;
export const logoutEndpoint = `${serverEndpoint}/oauth/logout`;
