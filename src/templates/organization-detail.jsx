import React from 'react';
import { graphql } from 'gatsby';
import PropTypes from 'prop-types';
import Layout from '../components/layout';
import ContactDetail from '../components/organization-details/contact-detail';
import LocationDetail from '../components/organization-details/location-detail';
import ServiceDetail from '../components/organization-details/service-detail';
import ProgramDetail from '../components/organization-details/program-detail';

const OrganizationDetail = ({ data }) => {
  /** @namespace data.hsdaorganizations */
  const organization = data.hsdaorganizations;
  let contactDetails = 'Not Supplied';
  let locationDetails = 'Not Supplied';
  let serviceDetails = 'Not Supplied';
  let programDetails = 'Not Supplied';
  if (organization.contacts[0] != null) {
    contactDetails = organization.contacts.map(
      (singleContact, index) => (
        <ContactDetail
          key={singleContact.alternative_id}
          index={index}
          contact={singleContact}
        />
      ),
    );
  }

  if (organization.locations[0] != null) {
    locationDetails = organization.locations.map(
      (singleLocation, index) => (
        <LocationDetail
          key={singleLocation.alternative_id}
          index={index}
          location={singleLocation}
        />
      ),
    );
  }

  if (organization.services[0] != null) {
    serviceDetails = organization.services.map(
      (singleService, index) => (
        <ServiceDetail
          key={singleService.alternative_id + singleService.location_id}
          index={index}
          service={singleService}
        />
      ),
    );
  }

  if (organization.programs[0] != null) {
    programDetails = organization.programs.map(
      (singleProgram, index) => (
        <ProgramDetail
          key={singleProgram.alternative_id}
          index={index}
          program={singleProgram}
        />
      ),
    );
  }

  return (
    <Layout siteTitle={organization.name}>
      <h2>Organization Details</h2>
      <div>
Alternative Name
        {organization.alternate_name
        || '(not supplied)'}
      </div>
      <div>Description:</div>
      <p>{organization.description}</p>
      <div>
Email:
        {organization.email || '(not supplied)'}
      </div>
      <div>
Url:
        {organization.url || '(not supplied)'}
      </div>
      <div>
Tax Status:
        {organization.tax_status || '(not supplied)'}
      </div>
      <div>
Tax ID:
        {organization.tax_id || '(not supplied)'}
      </div>
      <div>
Year Incorporated:
        {organization.year_incorporated
        || '(not supplied)'}
      </div>
      <div>
Legal Status:
        {organization.legal_status || '(not supplied)'}
      </div>
      <br />
      <h2>Contacts</h2>
      {contactDetails}
      <h2>Locations</h2>
      {locationDetails}
      <h2>Services</h2>
      {serviceDetails}
      <h2>Programs</h2>
      {programDetails}
    </Layout>
  );
};

OrganizationDetail.propTypes = {
  data: PropTypes.objectOf(PropTypes.object),
};

OrganizationDetail.defaultProps = {
  data: {},
};


export const query = graphql`
query ($alternativeId: String!) {
  hsdaorganizations(alternative_id: {eq: $alternativeId}) {
    alternative_id,
    name,
    alternate_name,
    description,
    email,
    url,
    tax_status,
    tax_id,
    year_incorporated,
    legal_status,
    contacts {
          alternative_id
          organization_id
          service_id
          service_at_location_id
          name
          title
          department
          email
    },
    locations {
      alternative_id
      organization_id
      name
      alternate_name
      description
      transportation
      latitude
      longitude
    },
    services {
      alternative_id,
      organization_id,
      program_id,
      location_id,
      name,
      alternate_name,
      description,
      url,
      email,
      status,
      interpretation_services,
      application_process,
      wait_time,
      fees,
      accreditations,
      licenses
    },
    programs {
      alternative_id,
      organization_id,
      name
    }
  }
}
`;

export default OrganizationDetail;
