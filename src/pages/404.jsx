import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';

// FIXME: Hack from Github https://github.com/gatsbyjs/gatsby/issues/5329

const browser = typeof window !== 'undefined' && window;

const NotFoundPage = () => browser && (
  <Layout>
    <SEO title="404: Not found" />
    <h1>NOT FOUND</h1>
    <p>You just hit a route that does not exist. Please check your path and try again.</p>
  </Layout>
);

export default NotFoundPage;
