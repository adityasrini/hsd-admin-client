import React from 'react';
import { navigate, Router } from '@reach/router';
import Layout from '../components/layout';
import PrivateRoute from '../components/privateRoute';
import Profile from '../components/private-pages/profile';
import { isBrowser } from '../services/authentication';

const Edit = () => {
  if (isBrowser() && window.location.pathname === '/edit/') {
    navigate('/');
    return null;
  }
  return (
    <Layout>
      <Router>
        <PrivateRoute path="/edit/profile" component={Profile} />
      </Router>
    </Layout>
  );
};

export default Edit;
