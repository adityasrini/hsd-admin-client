import React from 'react';
import Layout from '../components/layout';

const AboutPage = () => (
  <Layout>
    <h1>About</h1>
    <section className="about-section">
      <p>
        The DC Department of Human Services (DHS) is committed to ensuring that clients
        experiencing homelessness (and the providers who serve them) have an authoritative,
        accurate, and governed source of information about homeless services available in the
        District of Columbia.
      </p>
      <p>
        Using the Human Services Data Standard (HSDS), the nationally recognized data standard for
        human services, DHS is transforming how providers can publish information about their
        organizations and services. This site offers all District homeless service providers the
        opportunity to keep their information up to date and consistently aligned with other service
        providers.
      </p>
      <p>
        Behind the scenes, there is a team dedicated DHS staff and volunteers ensuring this
        resource directory complies with HSDS governance standards while simultaneously customizing
        features for our continuum of care. Information in this directory will be made available to
        application developers to help build seamless navigation tools for clients and providers
        trying hard to navigate our complex system.
      </p>
      <p>
        If you have questions about this directory or how to use it, please reach out and contact
        us at
        {' '}
        <a href="mailto:hersh.gupta@dc.gov" className="email-link">hersh.gupta@dc.gov</a>
        . We are eager to hear your feedback and make continuous improvements to this tool.
      </p>
    </section>
  </Layout>
);

export default AboutPage;
