import React from 'react';
import { graphql, Link } from 'gatsby';
import PropTypes from 'prop-types';
import Layout from '../components/layout';

/** @namespace data.allHsdaorganizations.edges */
const OrganizationsIndexPage = ({ data }) => (
  <Layout>
    <h1>Organization name</h1>
    <Link to="/about" className="organization-suggestion">Suggest an Organization to add.</Link>
    <section className="organization-index">
      {data.allHsdaorganizations.edges.map(({ node }) => (
        <Link to={`/${node.alternative_id}`} key={node.alternative_id} className="organization-name">{node.name}</Link>
      ))}
    </section>
  </Layout>
);

OrganizationsIndexPage.propTypes = {
  data: PropTypes.objectOf(PropTypes.object),
};

OrganizationsIndexPage.defaultProps = {
  data: {},
};

export default OrganizationsIndexPage;

export const query = graphql`
query OrganizationsIndexQuery {
  allHsdaorganizations {
    edges {
      node {
      alternative_id, name
      }
    }
  }
}`;
