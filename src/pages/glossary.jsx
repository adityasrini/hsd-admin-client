import React from 'react';
import Layout from '../components/layout';
import '../components/glossary.css';

const GlossaryPage = () => (
  <Layout pageClass="glossary">
    <h1>Glossary of terms used in this directory</h1>
    <dl>
      <h2 className="definition-heading">Organization Details</h2>
      <dt>Name</dt>
      <dd>The official or public name of the organization.</dd>
      <dt>Alternate Name</dt>
      <dd>Alternative or commonly used name of the organization.</dd>
      <dt>Description</dt>
      <dd>
        A brief summary about the organization. It can contain markup such as HTML or Markdown.
      </dd>
      <dt>Email</dt>
      <dd>The contact e-mail address for the organization.</dd>
      <dt>URL</dt>
      <dd>The URL (website address) of the organization.</dd>
      <dt>Tax Status</dt>
      <dd>Government assigned tax designation for tax-exempt organizations.</dd>
      <dt>Tax ID</dt>
      <dd>A government issued identifier used for the purpose of tax administration.</dd>
      <dt>Year Incorporated</dt>
      <dd>The year in which the organization was legally formed.</dd>
      <dt>Legal Status</dt>
      <dd>
        The legal status defines the conditions that an organization is operating under; e.g.
        non-profit, private corporation or a government organization.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Program Details</h2>
      <dt>Name</dt>
      <dd>The name of the program.</dd>
      <dt>Alternate Name</dt>
      <dd>An alternative name for the program.</dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Service Details</h2>
      <dt>Name</dt>
      <dd>The official or public name of the service.</dd>
      <dt>Alternate Name</dt>
      <dd>Alternative or commonly used name for a service.</dd>
      <dt>Description</dt>
      <dd>A description of the service.</dd>
      <dt>URL</dt>
      <dd>URL of the service.</dd>
      <dt>Email</dt>
      <dd>Email address for the service.</dd>
      <dt>Status</dt>
      <dd>The current status of the service.</dd>
      <dt>Interpretation Services</dt>
      <dd>A description of any interpretation services available for accessing this service.</dd>
      <dt>Application Process</dt>
      <dd>The steps needed to access the service.</dd>
      <dt>Wait Time</dt>
      <dd>Time a client may expect to wait before receiving a service.</dd>
      <dt>Fees</dt>
      <dd>Details of any charges for service users to access this service.</dd>
      <dt>Accreditations</dt>
      <dd>
        Details of any accreditations. Accreditation is the formal evaluation of an organization or
        program against best practice standards set by an accrediting organization.
      </dd>
      <dt>Licenses</dt>
      <dd>
        An organization may have a license issued by a government entity to operate legally. A list
        of any such licenses can be provided here.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Location Details</h2>
      <dt>Name</dt>
      <dd>The name of the location.</dd>
      <dt>Alternate Name</dt>
      <dd>An alternative name for the location.</dd>
      <dt>Description</dt>
      <dd>A description of this location.</dd>
      <dt>Transportation</dt>
      <dd>
        A description of the access to public or private transportation to and from the location.
      </dd>
      <dt>Latitude</dt>
      <dd>Y coordinate of location expressed in decimal degrees in WGS84 datum.</dd>
      <dt>Longitude</dt>
      <dd>X coordinate of location expressed in decimal degrees in WGS84 datum.</dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Phone Details</h2>
      <dt>Phone Number</dt>
      <dd>The phone number.</dd>
      <dt>Extension</dt>
      <dd>The extension of the phone number.</dd>
      <dt>Type</dt>
      <dd>
        Indicates the type of phone service, e.g. text (for SMS), voice, fax, cell, video, pager,
        textphone.
      </dd>
      <dt>Language</dt>
      <dd>
        Languages are listed as two letter abbreviations e.g. &#39;en&#39; for English, &#39;es&#39;
        for Spanish, &#39;am&#39; for Amharic, &#39;zh&#39; for Chinese.
      </dd>
      <dt>Description</dt>
      <dd>
        A description providing extra information about the phone service (e.g. any special
        arrangements for accessing, or details of availability at particular times.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Contact Details</h2>
      <dt>Name</dt>
      <dd>The name of the person.</dd>
      <dt>Title</dt>
      <dd>The job title of the person.</dd>
      <dt>Department</dt>
      <dd>
        The department that the person is part of.
      </dd>
      <dt>Email</dt>
      <dd>The email address of the person.</dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Address Details</h2>
      <dt>Attention</dt>
      <dd>
        The person or entity whose attention should be sought at the location (Often included as
        &#39;care of&#39; component of an address).
      </dd>
      <dt>Address</dt>
      <dd>The first line(s) of the address, including office, building number and street.</dd>
      <dt>City</dt>
      <dd>The city in which the address is located.</dd>
      <dt>Region</dt>
      <dd>The region in which the address is located (optional).</dd>
      <dt>State</dt>
      <dd>The state or province in which the address is located.</dd>
      <dt>ZIP Code</dt>
      <dd>The postal code for the address.</dd>
      <dt>Country</dt>
      <dd>
        The country in which the address is located. This should be given as a two letter
        abbreviation.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Postal Address Details</h2>
      <dt>Attention</dt>
      <dd>
        The person or entity whose attention should be sought at the location (Often included as
        &#39;care of&#39; component of an address).
      </dd>
      <dt>Address</dt>
      <dd>The first line(s) of the address, including office, building number and street.</dd>
      <dt>City</dt>
      <dd>The city in which the address is located.</dd>
      <dt>Region</dt>
      <dd>The region in which the address is located (optional).</dd>
      <dt>State</dt>
      <dd>The state or province in which the address is located.</dd>
      <dt>ZIP Code</dt>
      <dd>The postal code for the address.</dd>
      <dt>Country</dt>
      <dd>
        The country in which the address is located. This should be given as a two letter
        abbreviation.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Regular Schedule Details</h2>
      <dt>Weekdays</dt>
      <dd>The day of the week that this entry relates to.</dd>
      <dt>Opens At</dt>
      <dd>
        The time when a service or location opens. This should use HH:MM format and should include
        timezone information.
      </dd>
      <dt>Closes At</dt>
      <dd>
        The time when a service or location closes. This should use HH:MM format and should include
        timezone information.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Holiday Schedule Details</h2>
      <dt>Closed</dt>
      <dd>Indicates if a service or location is closed during a public holiday.</dd>
      <dt>Opens At</dt>
      <dd>
        The time when a service or location opens. This should use HH:MM format and should include
        timezone information.
      </dd>
      <dt>Closes At</dt>
      <dd>
        The time when a service or location closes. This should use HH:MM format and should include
        timezone information.
      </dd>
      <dt>Start Date</dt>
      <dd>
        The first day that a service or location is closed during a public or private holiday.
      </dd>
      <dt>End Date</dt>
      <dd>
        The last day that a service or location is closed during a public or private holiday.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Funding Details</h2>
      <dt>Source</dt>
      <dd>A free text description of the source of funds for this organization or service.</dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Eligibility Details</h2>
      <dt>Eligibility</dt>
      <dd>The rules or guidelines that determine who can receive the service.</dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Service Area Details</h2>
      <dt>Service Area</dt>
      <dd>
        The geographic area where a service is available. This is a free-text description, and so
        may be precise or indefinite as necessary.
      </dd>
      <dt>Description</dt>
      <dd>
        A more detailed description of this service area. Used to provide any additional information
        that cannot be communicated using the structured area and geometry fields.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Required Document Details</h2>
      <dt>Document(s)</dt>
      <dd>
        The document required to apply for or receive the service. e.g. &#39;Government-issued
        ID&#39;, &#39;EU Passport&#39;.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Payment Accepted Details</h2>
      <dt>Payment(s) Accepted</dt>
      <dd>The methods of payment accepted for the service.</dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Language Details</h2>
      <dt>Language</dt>
      <dd>
        Languages are listed as two letter abbreviations e.g. &#39;en&#39; for English, &#39;es&#39;
        for Spanish, &#39;am&#39; for Amharic, &#39;zh&#39; for Chinese.
      </dd>
    </dl>

    <dl>
      <h2 className="definition-heading">Accessibility for Disabilities Details</h2>
      <dt>Accessibility</dt>
      <dd>
        Description of assistance or infrastructure that facilitate access to clients with
        disabilities.
      </dd>
      <dt>Details</dt>
      <dd>
        Any further details relating to the relevant accessibility arrangements at this location.
        E.g. whether advance notice is required to use an accessibility facility.
      </dd>
    </dl>
  </Layout>
);

export default GlossaryPage;
