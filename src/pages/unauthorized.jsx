import React from 'react';
import Layout from '../components/layout';

const Unauthorized = () => (
  <Layout>
    <div>
      <h3>
        You are not authorized to access this resource.
      </h3>
    </div>
  </Layout>
);

export default Unauthorized;
