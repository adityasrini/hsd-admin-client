import React from 'react';
import { graphql, StaticQuery } from 'gatsby';
import Img from 'gatsby-image';
import PropTypes from 'prop-types';

const findImage = (data, imageName) => (
  data.allImageSharp.edges.find(edge => edge.node.fluid.originalName === imageName).node.fluid);

const Image = ({ imageName, className }) => (
  <StaticQuery
    query={graphql`
          {
      allImageSharp {
        edges{
          node{
            fluid{
              originalName
            }
          }
        }
      }
    }
    `}
    render={data => (
      <Img
        fluid={findImage(data, imageName)}
        className={className}
      />
    )}
  />
);

Image.defaultProps = {
  imageName: '',
  className: '',
};

Image.propTypes = {
  imageName: PropTypes.string,
  className: PropTypes.string,
};

export default Image;
