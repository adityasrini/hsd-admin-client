import React from 'react';
import PropTypes from 'prop-types';

import Header from './header';
import './layout.css';
import SEO from './seo';
import Footer from './footer';

const Layout = ({
  children, siteTitle, backLink, pageClass,
}) => (

  <div className={`layout ${pageClass}`}>
    <SEO
      title="Homeless Services Directory"
      keywords={['homeless services directory',
        'DC Department of Human Services', 'react']}
    />
    <Header backLink={backLink} siteTitle={siteTitle} />
    <main>
      {children}
    </main>
    <Footer />
  </div>

);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  siteTitle: PropTypes.string,
  backLink: PropTypes.string,
  pageClass: PropTypes.string,
};

Layout.defaultProps = {
  siteTitle: 'Homeless Services Directory',
  backLink: '/',
  pageClass: '',
};

export default Layout;
