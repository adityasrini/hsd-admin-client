import React from 'react';
import PropTypes from 'prop-types';

const LocationDetail = ({ location, index }) => (
  <div style={{ marginBottom: '0.8rem' }}>
    <h4 style={{ marginBottom: '0.5rem' }}>
Location
      {index + 1}
    </h4>
    <div>
Name:
      {location.name}
    </div>
    <div>
Alternate name:
      {location.alternate_name || 'Not Applicable'}
    </div>
    <div>
Description:
      {location.description}
    </div>
    <div>
Transportation:
      {location.transportation || '(not available)'}
    </div>
    <div>
Co-ordinates: Latitude -
      {location.latitude}
      {' '}
Longitude -
      {location.longitude}
    </div>
  </div>
);

LocationDetail.propTypes = {
  location: PropTypes.objectOf(PropTypes.string),
  index: PropTypes.number,
};

LocationDetail.defaultProps = {
  location: {},
  index: 0,
};

export default LocationDetail;
