import React from 'react';
import PropTypes from 'prop-types';

const ProgramDetail = ({ program, index }) => (
  <div style={{ marginBottom: '0.8rem' }}>
    <h4 style={{ marginBottom: '0.5rem' }}>
Program
      {index + 1}
    </h4>
    <div>
Name:
      {program.name}
    </div>
  </div>
);

ProgramDetail.propTypes = {
  program: PropTypes.objectOf(PropTypes.string),
  index: PropTypes.number,
};

ProgramDetail.defaultProps = {
  program: {},
  index: 0,
};


export default ProgramDetail;
