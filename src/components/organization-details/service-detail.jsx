import React from 'react';
import PropTypes from 'prop-types';

const ServiceDetail = ({ service, index }) => (
  <div style={{ marginBottom: '0.8rem' }}>
    <h4 style={{ marginBottom: '0.5rem' }}>
Service
      {index + 1}
    </h4>
    <div>
Name:
      {service.name}
    </div>
    <div>
Alternate name:
      {service.alternate_name || 'Not Applicable'}
    </div>
    <div>
Description:
      {service.description || '(not supplied)'}
    </div>
    <div>
URL:
      {service.url || '(not available)'}
    </div>
    <div>
Email:
      {service.email || '(not available)'}
    </div>
    <div>
Status:
      {service.status}
    </div>
    <div>
Interpretation Services:
      {service.interpretation_services}
    </div>
    <div>
Application Process:
      {service.application_process || '(not available)'}
    </div>
    <div>
Wait Time:
      {service.wait_time || '(not available)'}
    </div>
    <div>
Fees:
      {service.fees || '(not available)'}
    </div>
    <div>
Accreditations:
      {service.accreditations || '(not available)'}
    </div>
    <div>
Licenses:
      {service.licenses || '(not available)'}
    </div>
  </div>
);

ServiceDetail.propTypes = {
  service: PropTypes.objectOf(PropTypes.string),
  index: PropTypes.number,
};

ServiceDetail.defaultProps = {
  service: {},
  index: 0,
};

export default ServiceDetail;
