import React from 'react';
import PropTypes from 'prop-types';

const ContactDetail = ({ contact, index }) => (
  <div style={{ marginBottom: '0.8rem' }}>
    <h4 style={{ marginBottom: '0.5rem' }}>
        Contact
      {index + 1}
    </h4>
    <div>
        Name:
      {contact.name}
    </div>
    <div>
        Title:
      {contact.title}
    </div>
    <div>
        Department:
      {contact.department}
    </div>
    <div>
        Email:
      {contact.email}
    </div>
  </div>
);

ContactDetail.propTypes = {
  contact: PropTypes.objectOf(PropTypes.string),
  index: PropTypes.number,
};

ContactDetail.defaultProps = {
  contact: {},
  index: 0,
};

export default ContactDetail;
