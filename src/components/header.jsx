import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';
import dcGovLogo from '../images/dc-gov-logo.svg';
import './header.css';
import { initiateLogin, isLoggedIn } from '../services/authentication';
import { loginEndpoint } from '../services/endpoints';

const Header = ({ siteTitle, backLink }) => (
  <header>
    <nav>
      <picture>
        <object data={dcGovLogo} className="dc-gov-logo" aria-label="dc government logo" />
      </picture>
      <Link to={backLink} className="page-title">
        {siteTitle}
      </Link>
      <div className="navlink-spacer" />
      <Link to="/glossary" className="navlink">
          Glossary
      </Link>
      <Link to="/about" className="navlink">
        About
      </Link>
      {
        isLoggedIn()
          ? (
            <a href="/edit/profile" className="signin-navlink">
              Provider
            </a>
          )
          : (
            <a href={loginEndpoint} onClick={initiateLogin} className="signin-navlink">
              Sign in
            </a>
          )
      }
    </nav>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
  backLink: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: '',
  backLink: '/',
};

export default Header;
