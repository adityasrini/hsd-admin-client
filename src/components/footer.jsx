import React from 'react';
import './footer.css';
import { Link } from '@reach/router';
import twitterLogo from '../images/twitter-logo-white.svg';
import { initiateLogin, isLoggedIn } from '../services/authentication';
import { loginEndpoint } from '../services/endpoints';


const Footer = () => (
  <footer>
    <div className="credits-container">
      <span className="footer-title">Homeless Services Directory</span>
      <div className="credits">
        <p>
        Designed for D.C. by
          {' '}
          <em>Aditya</em>
        ,
          {' '}
          <em>Four</em>
        , and
        </p>
        <p>
          {' '}
          <em>Hersh</em>
          {' '}
        supported by
          {' '}
          <em>Code for D.C.</em>
          {' '}
        and
        </p>
        <p>D.C. Department of Human Services</p>
      </div>
      <a href="https://dhs.dc.gov">DHS.DC.GOV</a>
      <a href="https://dc.gov">DC.GOV</a>
    </div>
    <div className="footer-navlinks">
      <Link to="about">About</Link>
      {
        isLoggedIn()
          ? (
            <a href="/edit/profile" className="signin-navlink">
              Provider
            </a>
          )
          : (
            <a href={loginEndpoint} onClick={initiateLogin} className="signin-navlink">
              Sign in
            </a>
          )
      }
      <Link to="glossary">Glossary</Link>
      <Link to="/about">Request Changes</Link>
      <Link to="/about">Submit feedback</Link>
    </div>
    <div className="footer-social-links">
      <a href="https://twitter.com">
        <object data={twitterLogo} className="twitter-logo-object" aria-label="twitter logo" />
      </a>
    </div>
  </footer>
);

export default Footer;
