import React from 'react';
import PropTypes from 'prop-types';
import { isLoggedIn, notAuthenticated } from '../services/authentication';

const PrivateRoute = ({ component: Component, ...rest }) => {
  if (!isLoggedIn()) {
    notAuthenticated();
    return null;
  }
  return <Component {...rest} />;
};

PrivateRoute.propTypes = {
  component: PropTypes.any.isRequired,
};

export default PrivateRoute;
