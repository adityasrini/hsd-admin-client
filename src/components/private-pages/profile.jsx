import React, { Component } from 'react';
import { isLoggedIn, logout, notAuthenticated } from '../../services/authentication';
import { authorizedEndpoint } from '../../services/endpoints';
import './profile.css';

class Profile extends Component {
  state = {
    userDetails: {
      name: '',
      email: '',
      organizationName: '',
    },
  };

  componentWillMount() {
    if (!isLoggedIn()) {
      notAuthenticated();
    }
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      fetch(authorizedEndpoint, { credentials: 'include' })
        .then((resp) => {
          if (resp.status >= 300 || resp.status < 200) {
            notAuthenticated();
          }
          return resp.json();
        })
        .then((parsedUserDetails) => {
          this.setState({
            userDetails: {
              name: parsedUserDetails.name,
              email: parsedUserDetails.email,
              organizationName: parsedUserDetails.organizationName,
            },
          });
        })
        .catch(err => console.log(err));
    }
  }

  profileArticle(userDetails) {
    return (
      <article data-tab-content="profile">
        <table>
          <tbody>
            <tr>
              <td>
              Name
              </td>
              <td>
                {userDetails.name}
              </td>
              <td>
              Edit
              </td>
            </tr>
            <tr>
              <td>
              Email
              </td>
              <td>
                {userDetails.email}
              </td>
              <td>
              Edit
              </td>
            </tr>
            <tr>
              <td>
              Password
              </td>
              <td>
              ******
              </td>
              <td>
              Edit
              </td>
            </tr>
            <tr>
              <td>
              Provider
              </td>
              <td>
                {userDetails.organizationName}
              </td>
            </tr>
          </tbody>
        </table>
      </article>
    );
  }

  render() {
    const { userDetails } = this.state;

    return (
      <div>
        <h1>SETTINGS</h1>
        <section className="user-details-container">
          <aside>
            <button type="button" data-tab="profile">Profile</button>
          </aside>
          {this.profileArticle(userDetails)}
          <button className="sign-out" type="button" onClick={logout}>Sign Out</button>
        </section>
      </div>
    );
  }
}

export default Profile;
