const path = require('path');

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  return graphql(`
{
  allHsdaorganizations {
    edges {
      node {
        alternative_id
      }
    }
  }
}`).then((result) => {
    result.data.allHsdaorganizations.edges.forEach(({ node }) => {
      if (node.alternative_id !== null) {
        createPage({
          path: `/${node.alternative_id}`,
          component: path.resolve('./src/templates/organization-detail.jsx'),
          context: {
            alternativeId: node.alternative_id,
          },
        });
      }
    });
  });
};

exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions;
  if (page.path.match(/^\/edit/)) {
    page.matchPath = '/edit/*';
    createPage(page);
  }
};
