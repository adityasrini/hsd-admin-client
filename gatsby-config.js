require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    title: 'DC Homeless Services Directory',
    description: 'The Homeless Services Directory for Washington DC.',
    author: 'Aditya Srinivasan gitlab.com/adityasrini',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/images`,
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'DC Homeless Services Directory',
        short_name: 'DC hsd',
        start_url: '/',
        background_color: '#FFFFFF',
        theme_color: '#FFFFFF',
        display: 'fullscreen',
        icon: 'src/images/dc-gov-logo.svg',
      },
    },

    {
      resolve: 'gatsby-source-apiserver',
      options: {
        typePrefix: 'hsda',
        url: `${process.env.GATSBY_API_SERVER_ENDPOINT}/organizations/full`,
        method: 'get',
        schemaType: {
          alternate_name: 'String',
          email: 'String',
          tax_status: 'String',
          tax_id: 'String',
          year_incorporated: 'String',
          legal_status: 'String',
          contacts: [{
            alternative_id: 'String',
            organization_id: 'String',
            service_id: 'String',
            service_at_location_id: 'String',
            name: 'String',
            title: 'String',
            department: 'String',
            email: 'String',
          }],
          locations: [{
            alternative_id: 'String',
            organization_id: 'String',
            name: 'String',
            alternate_name: 'String',
            description: 'String',
            transportation: 'String',
            latitude: 'String',
            longitude: 'String',
          }],
          services: [{
            alternative_id: 'String',
            organization_id: 'String',
            program_id: 'String',
            location_id: 'String',
            name: 'String',
            alternate_name: 'String',
            description: 'String',
            url: 'String',
            email: 'String',
            status: 'String',
            interpretation_services: 'String',
            application_process: 'String',
            wait_time: 'String',
            fees: 'String',
            accreditations: 'String',
            licenses: 'String',
          }],
          programs: [{
            alternative_id: 'String',
            organization_id: 'String',
            name: 'String',
          }],
        },
        headers: {
          'Content-Type': 'application/json',
        },
        name: 'organizations',
      },
    },
  ],
};
